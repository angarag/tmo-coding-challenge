import { StocksAppConfig } from '@coding-challenge/stocks/data-access-app-config';

export const environment: StocksAppConfig = {
  production: true,
  apiKey: 'pk_397f7af84d8b4ad6806de5cd00105e4d',
  apiURL: 'https://cloud.iexapis.com'
};
