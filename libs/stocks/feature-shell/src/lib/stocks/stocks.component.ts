import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { Observable } from 'rxjs';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {
  stockPickerForm: FormGroup;
  symbol: string;
  period: string;
  picker: any;
  quotes$: Observable<any>;
  startDate = new Date(2019, 0, 1);
  maxDate1: Date;
  minDate2: Date;
  timePeriods = [
    { viewValue: 'All available data', value: 'max' },
    { viewValue: 'Five years', value: '5y' },
    { viewValue: 'Two years', value: '2y' },
    { viewValue: 'One year', value: '1y' },
    { viewValue: 'Year-to-date', value: 'ytd' },
    { viewValue: 'Six months', value: '6m' },
    { viewValue: 'Three months', value: '3m' },
    { viewValue: 'One month', value: '1m' }
  ];

  constructor(private fb: FormBuilder, private priceQuery: PriceQueryFacade) {
    this.stockPickerForm = fb.group({
      symbol: [null, Validators.required],
      from: [null, Validators.required],
      to: [null, Validators.required]
    });
    this.maxDate1 = new Date();
    this.minDate2 = null;
    console.log(this.maxDate1);
  }

  ngOnInit() {
    console.log('onInit');
    this.quotes$ = this.priceQuery.priceQueries$;
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }
  fetchQuote(timeFrame: any = false) {
    console.log(this.stockPickerForm.value);
    if (this.stockPickerForm.valid) {
      console.log('form is valid');
      const { symbol, period, from, to } = this.stockPickerForm.value;
      this.priceQuery.fetchQuote(symbol, period, from, to);
    } else if (!timeFrame) {
      this.handleTimeFrameChange(timeFrame);
    }
  }
  handleSymbol() {
    console.log('symbol changed:', this.stockPickerForm.value.symbol);
    this.fetchQuote();
  }
  handleDateChange(date: number, event: MatDatepickerInputEvent<Date>) {
    if (date == 0) this.minDate2 = new Date(event.value);
    console.log(`${date},${event.value}`);
  }
  handleTimeFrameChange(timeFrame: string) {
    console.log(timeFrame);
    const { symbol } = this.stockPickerForm.value;
    this.priceQuery.fetchQuote(symbol, timeFrame, undefined, undefined);
  }
}
